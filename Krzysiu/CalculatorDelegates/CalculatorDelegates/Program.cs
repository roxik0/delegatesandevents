﻿using System;

namespace CalculatorDelegates
{
    class Program
    {
        static void Main(string[] args)
        {
            double a = 5;
            double b = 2931923;

            //Calculates calculates = new Calculates();
            //calculates.Operation = MathOperations.Add;
            //calculates.Calculate(a, b);
            //calculates.Operation = MathOperations.Divide;
            //calculates.Calculate(a, b);
            //calculates.Operation = MathOperations.Sub;
            //calculates.Calculate(a, b);
            //calculates.Operation = MathOperations.Multiply;
            //calculates.Calculate(a, b);
            Calculates calculates = new Calculates();
            calculates.Operation = MathOperations.Add;
            calculates.Calculate(a, b);
            calculates.Operation += MathOperations.Divide;
            calculates.Calculate(a, b);
            calculates.Operation += MathOperations.Sub;
            calculates.Calculate(a, b);
            calculates.Operation += MathOperations.Multiply;
            calculates.Calculate(a, b);
        }
    }
}
