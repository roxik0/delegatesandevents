﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CalculatorDelegates
{
    class MathOperations
    {
        public static void Add(double a, double b)
        {
            Console.WriteLine(a+b);
        }
        public static void Sub(double a, double b)
        {
            Console.WriteLine(a-b);
        }
        public static void Multiply(double a, double b)
        {
            Console.WriteLine(a*b);
        }
        public static void Divide(double a, double b)
        {
            Console.WriteLine(a/b);
        }
    }
}
