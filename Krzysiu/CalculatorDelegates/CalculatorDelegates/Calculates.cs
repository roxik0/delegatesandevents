﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace CalculatorDelegates
{
    delegate void CalculateOperation(double a, double b);
    class Calculates
    {
        
        public CalculateOperation Operation { get; set; }

        public void Calculate(double a, double b)
        {
            if (Operation != null)
            {
                
                Console.WriteLine($"Wynik działania {Operation.Method.Name} to:");
                //Console.WriteLine(Operation.Invoke(a, b));
                Operation.Invoke(a, b);
            }
        }
    }
}
