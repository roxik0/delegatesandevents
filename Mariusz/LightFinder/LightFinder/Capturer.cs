﻿using Emgu.CV;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace LightFinder
{
   public class Capturer
    {
        public delegate void Callback(Mat frame);
        public void Captur(Callback callback)
        {
            
            VideoCapture capture = new VideoCapture();
            capture.Start();

            while (true)
            {
                Mat frame = new Mat();
                capture.Retrieve(frame);
                if (!frame.IsEmpty)
                {
                    //frame.Save("frame.png");
                    var c = frame.GetRawData(240, 240);
                    var value = (c[0] + c[1] + c[2]) / 3;
                    Console.WriteLine(value);
                    if (value>200)
                    {
                        callback.Invoke(frame);
                    }
                }
                Console.WriteLine("Teraz robimy zdjęcie");
                Thread.Sleep(1000);
            }
        }
    }
}
