﻿using Emgu.CV;
using System;
using System.IO;
using System.Threading;

namespace LightFinder
{
    class Program
    {
        public static void Save(Mat mat)
        {
            Console.WriteLine("Smile");
            mat.Save("frame"+DateTime.Now.Ticks+".png");
        }

        public static void LogFile(Mat mat)
        {
            File.AppendAllLines("Log.txt", new[] { DateTime.Now.ToLongTimeString() });
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            var capture = new Capturer();
            Capturer.Callback callBacks;
            callBacks = Save;
            callBacks += LogFile;

            capture.Captur(callBacks); 
            
            
        }
    }
}
