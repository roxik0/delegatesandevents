﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace CallbackExample
{
    class Program
    {
        static bool FindDuda(string value)
        {
            return value.EndsWith("Bosak");

        }
        static void Callback(string value)
        {
            Console.WriteLine("Zakończ wybory w I turze");

        }

        static void Main(string[] args)
        {
            var namesOfCandidates = new List<string>()
            {
                "Paweł Tanajno",
                "Krzysztof Bosak",
                "Andrzej Duda",
                "Stanisław Żółtek",
                "Robert Biedroń",


            };
            
            Func<string,bool> wrongChoicesOfPeople;
            wrongChoicesOfPeople = val => val.EndsWith("k") || val.Contains("o");

             Finder finder= new Finder();
             finder.Find(namesOfCandidates,FindDuda,Callback);
            finder.Find(namesOfCandidates, wrongChoicesOfPeople, founded => { Console.WriteLine($"Skreśl: {founded} z listy");});
            
            Console.ReadKey();
        }

        
    }
    
    //delegate void FindCallback<T>(T founded);
    //delegate bool FindFilter<T>(T value);
}
