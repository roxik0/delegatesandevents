﻿using System;
using System.Collections.Generic;

namespace CallbackExample
{
    class Finder
    {
        //public void Find<T>(List<T> listOfNames,FindFilter<T> filter,  FindCallback<T> callback)
        public void Find<T>(List<T> listOfNames, Func<T,bool> filter, Action<T> callback)
        {
            foreach (var name in listOfNames)
            {
                if (filter(name))
                {
                    callback?.Invoke(name);
                
                }
            }
        }
    }
}