﻿using System;

namespace ExampleEvents
{
    public class ChoiceHandlerEventArgs : EventArgs
    {
        public string Choice { get; set; }
    }
    public delegate void OnProperChoiceHandler(ChoiceHandlerEventArgs args);


    public class ChoiceValidator
    {
        public event OnProperChoiceHandler OnProperChoice;
       
        public void ValidateChoice(string name)
        {
            if (name == "Andrzej Duda")
            {
                OnProperChoice?.Invoke(new ChoiceHandlerEventArgs(){Choice = name});
            }
        }
    }
}