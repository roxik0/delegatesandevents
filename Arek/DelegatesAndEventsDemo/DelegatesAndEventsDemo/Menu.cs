﻿using System.Collections.Generic;

namespace DelegatesAndEventsDemo
{
    class Menu
    {
        private readonly List<MenuItem> _menuItems;

        public Menu(List<MenuItem> menuItems)
        {
            _menuItems = menuItems;
        }
        public void PrintMenu()
        {
            for (int i = 0; i < _menuItems.Count; i++)
            {
                var menuItem = _menuItems[i];
                menuItem.Print(i+1);
            }
            
        }

        public void RunOperation(int operationNumber)
        {
            _menuItems[operationNumber - 1].Operation();
        }
    }
}