﻿using System;

namespace DelegatesAndEventsDemo
{
    class MenuItem
    {
        public delegate void OperationDelegate();

        public string Name { get; set; }
        public OperationDelegate Operation { get; set; }
        public void Print(int operationNumber)
        {
            Console.WriteLine($"{operationNumber}. {Name}");
        }
    }
}