﻿using System;
using System.Collections.Generic;

namespace DelegatesAndEventsDemo
{
    class Program
    {
        static void Exit()
        {
            Environment.Exit(0);
        }
        static void PrintSomething()
        {
            Console.WriteLine("Something");
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Calculator calc=new Calculator()
            {
                A=5,
                B=7,
            };
            List<MenuItem> items = new List<MenuItem>();
            items.Add(
                new MenuItem()
                {
                    Name = "Wypisz coś",
                    Operation = PrintSomething,
                }
            );
            items.Add(
                new MenuItem()
                {
                    Name = "Calculate",
                    Operation = calc.Calculate,
                }
            );
            items.Add(new MenuItem()
                {
                    Name = "Zakończ",
                    Operation = Exit,
                }
            );
            
            Menu mainMenu =new Menu(items);
            mainMenu.PrintMenu();            
            var operationNum = Convert.ToInt32(Console.ReadLine());
            mainMenu.RunOperation(operationNum);

            Console.ReadKey();
        }
    }
    
    public class Calculator
    {
        public int A { get; set; }
        public int B { get; set; }

        public void Calculate()
        {
            Console.WriteLine(A+B);
        }    
    }
}
